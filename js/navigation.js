/**
 * Created by jstgermain on 5/8/15.
 * Custom JS file for Off Screen Navigation Tutorial
 */

$(function() {

	/**
	 * Create clone of the header
	 * This allows you to place the element wherever you want
	 * while making it stick to the top of the screen on scroll
	 */
	$('header')
		.addClass('original')
		.clone()
		.insertAfter('header')
		.addClass('cloned')
		.removeClass('original')
		.css({'position': 'fixed', 'top': '0', 'z-index': '500'})
		.hide()
	;

	/**
	 * Set Interval so that if the element is placed
	 * somewhere other than the top of the screen to
	 * start, it will check the element position every
	 * 10 milliseconds, allowing you to stick an element
	 * once it has been scrolled to the top.
	 */
	scrollIntervalID = setInterval(stickyHeader, 10);

	/**
	 * @type {*|jQuery}
	 */
	var navWidth = $('nav').width();

	$('nav').css({
		'left': -navWidth
	});

	$('header').on('click', '.navbar-btn', function() {

		if ( !$(this).hasClass('active-menu') ) {

			$('nav').show().animate({'left': 0}, 'slow');
			$('body').css({'position': 'absolute', 'left': 0, 'width': $('body').width()}).animate({'left': navWidth}, 'slow');
			$(this).addClass('active-menu');

		} else {

			$('nav').animate({'left': -navWidth}, 'slow', function() {
				$(this).hide();
			});

			$('body').animate({'left': 0}, 'slow', function() {
				$(this).removeAttr('style');
			});

			$(this).removeClass('active-menu');

		}

	});

});


function stickyHeader() {

	/**
	 * @type {*|jQuery}
	 */
	var orgElementPos = $('.original').offset();

	orgElementTop = orgElementPos.top;

	if ($(window).scrollTop() >= (orgElementTop)) {

		/**
		 * Scrolled past the original position; now only show the cloned, sticky element.
		 * Cloned element should always have same left position and width as original element.
		 * @type {*|jQuery|HTMLElement}
		 */
		orgElement = $('.original');
		$('.cloned').css('top',0).css('width', '100%').show();
		$('.original').css('visibility','hidden');

	} else {

		/**
		 * Not scrolled past the menu
		 * Only show the original menu.
 		 */
		$('.cloned').hide();
		$('.original').css('visibility','visible');

	}

}